using System;
using CommandLibrary;
using CommandLibrary.Test;
using UnityEngine;

namespace CommandLibrary.UnitTests
{

[TestGroup]
public class TestMathExtensions
{
	[Test]
	public static void TestNlerp()
	{
		Quaternion start = Quaternion.Euler(90f, 0f, 45f);
		Quaternion finish = Quaternion.Euler(45f, 45f, 0f);
		TestRunner.AssertApprox(MathExtensions.Nlerp(start, finish, 0f), start, 0.001f);
		TestRunner.AssertApprox(MathExtensions.Nlerp(start, finish, 1f), finish, 0.001f);
		TestRunner.Assert(MathExtensions.Nlerp(start, finish, 1.5f) != finish);
		TestRunner.Assert(MathExtensions.Nlerp(start, finish, -0.5f) != start);

		float angleFromStart = 0f;
		float angleToFinish = Quaternion.Angle(start, finish);
		float angleDiff = Quaternion.Angle(start, finish);

		// As t -> 1, the angle from start should increase, and the angle to finish should decrease.
		for (int i = 0; i < 10; i++) {
			float t = i * 0.1f;
			Quaternion newRotation = MathExtensions.Nlerp(start, finish, t);

			float newAngleFromStart = Quaternion.Angle(start, newRotation);
			float newAngleToFinish = Quaternion.Angle(newRotation, finish);

			TestRunner.AssertLessThanOrEqual(angleFromStart, newAngleFromStart);
			TestRunner.AssertLessThanOrEqual(angleToFinish, angleToFinish);
			TestRunner.AssertApprox(angleDiff, newAngleFromStart + newAngleToFinish, 0.01f);

			angleFromStart = newAngleFromStart;
			angleToFinish = newAngleToFinish;
		}
	}
}

}

