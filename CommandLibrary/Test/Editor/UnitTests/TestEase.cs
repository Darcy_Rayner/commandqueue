using System;
using CommandLibrary;
using CommandLibrary.Test;

namespace CommandLibrary.UnitTests
{

[TestGroup]
public class TestEase
{
	[Test]	
	public static void TestEaseValid()
	{
		// Test easing identity, f(0) = 0, f(1) = 1.
		bool threwException = false;
		try {Ease.CeilStep(0); } catch (ArgumentOutOfRangeException) {threwException = true;}
		TestRunner.Assert(threwException, "CeilStep input 0 invalid.");
		TestRunner.AssertApprox(Ease.CeilStep(5)(0.0f), 0.0f, 0.001f, "CeilStep");
		TestRunner.AssertApprox(Ease.CeilStep(5)(1.0f), 1.0f, 0.001f, "CeilStep");
		TestRunner.AssertApprox(Ease.CeilStep(1)(0.0f), 0.0f, 0.001f, "CeilStep");
		TestRunner.AssertApprox(Ease.CeilStep(1)(1.0f), 1.0f, 0.001f, "CeilStep");
		
		threwException = false;
		try {Ease.FloorStep(0); } catch (ArgumentOutOfRangeException) {threwException = true;}
		TestRunner.Assert(threwException, "FloorStep input 0 invalid.");
		TestRunner.AssertApprox(Ease.FloorStep(5)(0.0f), 0.0f, 0.001f, "FloorStep");
		TestRunner.AssertApprox(Ease.FloorStep(5)(1.0f), 1.0f, 0.001f, "FloorStep");
		TestRunner.AssertApprox(Ease.FloorStep(1)(0.0f), 0.0f, 0.001f, "FloorStep");
		TestRunner.AssertApprox(Ease.FloorStep(1)(1.0f), 1.0f, 0.001f, "FloorStep");
		
		threwException = false;
		try {Ease.RoundStep(0); } catch (ArgumentOutOfRangeException) {threwException = true;}
		TestRunner.Assert(threwException, "RoundStep input 0 invalid.");
		TestRunner.AssertApprox(Ease.RoundStep(5)(0.0f), 0.0f, 0.001f, "RoundStep");
		TestRunner.AssertApprox(Ease.RoundStep(5)(1.0f), 1.0f, 0.001f, "RoundStep");
		TestRunner.AssertApprox(Ease.RoundStep(1)(0.0f), 0.0f, 0.001f, "RoundStep");
		TestRunner.AssertApprox(Ease.RoundStep(1)(1.0f), 1.0f, 0.001f, "RoundStep");
		
		TestRunner.AssertApprox(Ease.Elastic(20.0f, 20.0f)(0.0f), 0.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(0.01f, 20.0f)(0.0f), 0.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(20.0f, 0.01f)(0.0f), 0.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(0.01f, 0.01f)(0.0f), 0.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(-1.0f, 1.0f)(0.0f), 0.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(1.0f, -1.0f)(0.0f), 0.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(-1.0f, -1.0f)(0.0f), 0.0f, 0.001f, "Elastic");
		
		TestRunner.AssertApprox(Ease.Elastic(20.0f, 20.0f)(1.0f), 1.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(0.01f, 20.0f)(1.0f), 1.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(20.0f, 0.01f)(1.0f), 1.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(0.01f, 0.01f)(1.0f), 1.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(-1.0f, 1.0f)(1.0f), 1.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(1.0f, -1.0f)(1.0f), 1.0f, 0.001f, "Elastic");
		TestRunner.AssertApprox(Ease.Elastic(-1.0f, -1.0f)(1.0f), 1.0f, 0.001f, "Elastic");
		
		TestRunner.AssertApprox(Ease.InElastic()(0.0f), 0.0f, 0.001f, "InElastic");
		TestRunner.AssertApprox(Ease.InElastic()(1.0f), 1.0f, 0.001f, "InElastic");
		TestRunner.AssertApprox(Ease.OutElastic()(0.0f), 0.0f, 0.001f, "OutElastic");
		TestRunner.AssertApprox(Ease.OutElastic()(1.0f), 1.0f, 0.001f, "OutElastic");		
		TestRunner.AssertApprox(Ease.InOutElastic()(0.0f), 0.0f, 0.001f, "InOutElastic");
		TestRunner.AssertApprox(Ease.InOutElastic()(1.0f), 1.0f, 0.001f, "InOutElastic");
		
		TestRunner.AssertApprox(Ease.InBack(0.0f)(0.0f), 0.0f, 0.001f, "InBack");
		TestRunner.AssertApprox(Ease.InBack(0.2f)(0.0f), 0.0f, 0.001f, "InBack");
		TestRunner.AssertApprox(Ease.InBack(-0.2f)(0.0f), 0.0f, 0.001f, "InBack");
		TestRunner.AssertApprox(Ease.InBack(0.0f)(1.0f), 1.0f, 0.001f, "InBack");
		TestRunner.AssertApprox(Ease.InBack(0.2f)(1.0f), 1.0f, 0.001f, "InBack");
		TestRunner.AssertApprox(Ease.InBack(-0.2f)(1.0f), 1.0f, 0.001f, "InBack");
		
		TestRunner.AssertApprox(Ease.OutBack(0.0f)(0.0f), 0.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.OutBack(0.2f)(0.0f), 0.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.OutBack(-0.2f)(0.0f), 0.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.OutBack(0.0f)(1.0f), 1.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.OutBack(0.2f)(1.0f), 1.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.OutBack(-0.2f)(1.0f), 1.0f, 0.001f, "OutBack");
		
		TestRunner.AssertApprox(Ease.InOutBack(0.0f)(0.0f), 0.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.InOutBack(0.2f)(0.0f), 0.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.InOutBack(-0.2f)(0.0f), 0.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.InOutBack(0.0f)(1.0f), 1.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.InOutBack(0.2f)(1.0f), 1.0f, 0.001f, "OutBack");
		TestRunner.AssertApprox(Ease.InOutBack(-0.2f)(1.0f), 1.0f, 0.001f, "OutBack");
		
		TestRunner.AssertApprox(Ease.InBounce()(0.0f), 0.0f, 0.001f, "InBounce");
		TestRunner.AssertApprox(Ease.InBounce()(1.0f), 1.0f, 0.001f, "InBounce");	
		TestRunner.AssertApprox(Ease.OutBounce()(0.0f), 0.0f, 0.001f, "OutBounce");
		TestRunner.AssertApprox(Ease.OutBounce()(1.0f), 1.0f, 0.001f, "OutBounce");
		TestRunner.AssertApprox(Ease.InOutBounce()(0.0f), 0.0f, 0.001f, "InOutBounce");
		TestRunner.AssertApprox(Ease.InOutBounce()(1.0f), 1.0f, 0.001f, "InOutBounce");
		
		TestRunner.AssertApprox(Ease.InCirc()(0.0f), 0.0f, 0.001f, "InCirc");
		TestRunner.AssertApprox(Ease.InCirc()(1.0f), 1.0f, 0.001f, "InCirc");	
		TestRunner.AssertApprox(Ease.OutCirc()(0.0f), 0.0f, 0.001f, "OutCirc");
		TestRunner.AssertApprox(Ease.OutCirc()(1.0f), 1.0f, 0.001f, "OutCirc");
		TestRunner.AssertApprox(Ease.InOutCirc()(0.0f), 0.0f, 0.001f, "InOutCirc");
		TestRunner.AssertApprox(Ease.InOutCirc()(1.0f), 1.0f, 0.001f, "InOutCirc");
		
		TestRunner.AssertApprox(Ease.InCubic()(0.0f), 0.0f, 0.001f, "InCubic");
		TestRunner.AssertApprox(Ease.InCubic()(1.0f), 1.0f, 0.001f, "InCubic");	
		TestRunner.AssertApprox(Ease.OutCubic()(0.0f), 0.0f, 0.001f, "OutCubic");
		TestRunner.AssertApprox(Ease.OutCubic()(1.0f), 1.0f, 0.001f, "OutCubic");
		TestRunner.AssertApprox(Ease.InOutCubic()(0.0f), 0.0f, 0.001f, "InOutCubic");
		TestRunner.AssertApprox(Ease.InOutCubic()(1.0f), 1.0f, 0.001f, "InOutCubic");
		
		TestRunner.AssertApprox(Ease.InQuad()(0.0f), 0.0f, 0.001f, "InQuad");
		TestRunner.AssertApprox(Ease.InQuad()(1.0f), 1.0f, 0.001f, "InQuad");	
		TestRunner.AssertApprox(Ease.OutQuad()(0.0f), 0.0f, 0.001f, "OutQuad");
		TestRunner.AssertApprox(Ease.OutQuad()(1.0f), 1.0f, 0.001f, "OutQuad");
		TestRunner.AssertApprox(Ease.InOutQuad()(0.0f), 0.0f, 0.001f, "InOutQuad");
		TestRunner.AssertApprox(Ease.InOutQuad()(1.0f), 1.0f, 0.001f, "InOutQuad");
		
		TestRunner.AssertApprox(Ease.InQuart()(0.0f), 0.0f, 0.001f, "InQuart");
		TestRunner.AssertApprox(Ease.InQuart()(1.0f), 1.0f, 0.001f, "InQuart");	
		TestRunner.AssertApprox(Ease.OutQuart()(0.0f), 0.0f, 0.001f, "OutQuart");
		TestRunner.AssertApprox(Ease.OutQuart()(1.0f), 1.0f, 0.001f, "OutQuart");
		TestRunner.AssertApprox(Ease.InOutQuart()(0.0f), 0.0f, 0.001f, "InOutQuart");
		TestRunner.AssertApprox(Ease.InOutQuart()(1.0f), 1.0f, 0.001f, "InOutQuart");
		
		TestRunner.AssertApprox(Ease.InQuint()(0.0f), 0.0f, 0.001f, "InQuint");
		TestRunner.AssertApprox(Ease.InQuint()(1.0f), 1.0f, 0.001f, "InQuint");	
		TestRunner.AssertApprox(Ease.OutQuint()(0.0f), 0.0f, 0.001f, "OutQuint");
		TestRunner.AssertApprox(Ease.OutQuint()(1.0f), 1.0f, 0.001f, "OutQuint");
		TestRunner.AssertApprox(Ease.InOutQuint()(0.0f), 0.0f, 0.001f, "InOutQuint");
		TestRunner.AssertApprox(Ease.InOutQuint()(1.0f), 1.0f, 0.001f, "InOutQuint");
		
		TestRunner.AssertApprox(Ease.InExpo()(0.0f), 0.0f, 0.001f, "InExpo");
		TestRunner.AssertApprox(Ease.InExpo()(1.0f), 1.0f, 0.001f, "InExpo");	
		TestRunner.AssertApprox(Ease.OutExpo()(0.0f), 0.0f, 0.001f, "OutExpo");
		TestRunner.AssertApprox(Ease.OutExpo()(1.0f), 1.0f, 0.001f, "OutExpo");
		TestRunner.AssertApprox(Ease.InOutExpo()(0.0f), 0.0f, 0.001f, "InOutExpo");
		TestRunner.AssertApprox(Ease.InOutExpo()(1.0f), 1.0f, 0.001f, "InOutExpo");
		
		TestRunner.AssertApprox(Ease.InSin()(0.0f), 0.0f, 0.001f, "InSin");
		TestRunner.AssertApprox(Ease.InSin()(1.0f), 1.0f, 0.001f, "InSin");	
		TestRunner.AssertApprox(Ease.OutSin()(0.0f), 0.0f, 0.001f, "OutSin");
		TestRunner.AssertApprox(Ease.OutSin()(1.0f), 1.0f, 0.001f, "OutSin");
		TestRunner.AssertApprox(Ease.InOutSin()(0.0f), 0.0f, 0.001f, "InOutSin");
		TestRunner.AssertApprox(Ease.InOutSin()(1.0f), 1.0f, 0.001f, "InOutSin");
	
	}
}

}
