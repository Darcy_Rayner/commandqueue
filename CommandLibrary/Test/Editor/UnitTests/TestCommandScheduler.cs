using System;
using CommandLibrary;
using CommandLibrary.Test;

namespace CommandLibrary.UnitTests
{

[TestGroup]
public class TestCommandScheduler
{	
	[Test]
	public static void TestOrdering()
	{
		CommandScheduler scheduler = new CommandScheduler();
		
		int a = 0;
		scheduler.Add(
			Commands.Sequence(
				Commands.WaitForSeconds(1.0f),
				Commands.Do(() => ++a)
			)
		);
		
		int b = 0;
		scheduler.Add(
			Commands.Sequence(
				Commands.WaitForSeconds(1.0f),
				Commands.Do(() => ++b)
			)
		);
		
		scheduler.Add(
			Commands.Sequence(
				Commands.WaitForSeconds(1.5f),
				Commands.Do(() => ++b)
			)
		);
		TestRunner.AssertEqual(a, 0);
		TestRunner.AssertEqual(b, 0);
		scheduler.Update(1.0f);
		TestRunner.AssertEqual(a, 1);
		TestRunner.AssertEqual(b, 1);
		scheduler.Update(0.5f);
		TestRunner.AssertEqual(a, 1);
		TestRunner.AssertEqual(b, 2);
	}
}

}

