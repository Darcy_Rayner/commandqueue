using System;

namespace CommandLibrary.Test
{

[AttributeUsage(AttributeTargets.Class)]
public class TestGroupAttribute : System.Attribute
{
	public string Name;
	public TestGroupAttribute(string groupName = null) { Name = groupName; }
}

[AttributeUsage(AttributeTargets.Method)]
public class TestAttribute : System.Attribute
{
	public string Name;
	public TestAttribute(string testName = null) { Name = testName; }
}


}


