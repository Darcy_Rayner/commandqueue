using System;
using UnityEditor;
using UnityEngine;

namespace CommandLibrary.Test
{

public class TestMenu
{
	[MenuItem("CommandLibrary/Tests/Run Unit Tests")]
	public static void RunUnitTests()
	{
		TestRunner runner = TestRunner.GenerateTests();
		runner.OnLog = (message, isError) => {
			if (isError) {
				Debug.LogError(message);
			} else {
				Debug.Log(message);
			}
		};
		runner.SourcePath = Application.dataPath;

		var coroutine = runner.DoRun ();
		try {
			while (coroutine.MoveNext ()) {
				bool cancelled = EditorUtility.DisplayCancelableProgressBar(
					"Running Unit Tests",
					runner.CurrentTestName,
					runner.Progress
				);

				if (cancelled) {
					Debug.Log("Cancelled Unit Tests");
				}
			} 
		} catch (Exception e) {
			Debug.LogError("Uncaught exception : " + e);
		}
		EditorUtility.ClearProgressBar();
	}

}

}

